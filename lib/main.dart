import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:health_survey/result_page.dart';
import 'package:health_survey/sex_choice.dart';

import 'data.dart';
import 'my_box.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Health Survey',
      theme: ThemeData(
          primarySwatch: Colors.red,
          scaffoldBackgroundColor: Colors.redAccent.shade100),
      // home: MyHomePage(),
      initialRoute: MyHomePage.routeName,
      routes: {
        MyHomePage.routeName: (context) => MyHomePage(),
        ResultPage.route: (context) => ResultPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {

  static const String routeName = "/";
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Sex selectedSex;
  double sportRating = 0.0;
  double smokeRating = 0.0;
  int length = 170;
  int weight = 80;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text("Health Survey"),
        ),
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: MyBoxWidget(child: buildPlusMinusBox("Length")),
                  ),
                  Expanded(
                    child: MyBoxWidget(
                      child: buildPlusMinusBox("Weight"),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: MyBoxWidget(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "How much do you exercise per week?",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      sportRating.round().toString(),
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Slider(
                        min: 0,
                        max: 7,
                        divisions: 7,
                        value: sportRating,
                        onChanged: (double newRating) {
                          setState(() {
                            sportRating = newRating;
                          });
                        },
                        label: "$sportRating")
                  ],
                ),
              ),
            ),
            Expanded(
              child: MyBoxWidget(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "How much smoke do you smoke a day?",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      smokeRating.round().toString(),
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Slider(
                        min: 0,
                        max: 30,
                        value: smokeRating,
                        onChanged: (double newRating) {
                          setState(() {
                            smokeRating = newRating;
                          });
                        },
                        label: "$smokeRating")
                  ],
                ),
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: MyBoxWidget(
                      onPress: () {
                        setState(() {
                          selectedSex = Sex.FEMALE;
                        });
                      },
                      color:
                          selectedSex == Sex.FEMALE ? Colors.red : Colors.white,
                      child: SexChoice(
                        sex: Sex.FEMALE,
                        icon: FontAwesomeIcons.venus,
                      ),
                    ),
                  ),
                  Expanded(
                    child: MyBoxWidget(
                      onPress: () {
                        setState(() {
                          selectedSex = Sex.MALE;
                        });
                      },
                      color:
                          selectedSex == Sex.MALE ? Colors.red : Colors.white,
                      child: SexChoice(
                        sex: Sex.MALE,
                        icon: FontAwesomeIcons.mars,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ButtonTheme(
                  height: 50,
                  child: FlatButton(
                    onPressed: () {
                      var healthInput = HealthInput(
                          selectedSex, sportRating, smokeRating, length, weight);
                     /* Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ResultPage(healthInput)));*/
                      Navigator.pushNamed(context, ResultPage.route, arguments: healthInput);
                    },
                    child: Text("Calculate Lifetime", style: TextStyle( fontSize: 20),),
                    color: Colors.white,
                    highlightColor: Colors.redAccent,

                  ),
                ),
              ],
            )
          ],
        ));
  }

  Row buildPlusMinusBox(String label) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      RotatedBox(
        child: Text(label, style: TextStyle(fontSize: 20)),
        quarterTurns: -1,
      ),
      Text(label == 'Length' ? length.toString() : weight.toString(),
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ButtonTheme(
            minWidth: 40,
            child: OutlineButton(
              onPressed: () {
                setState(() {
                  label == 'Length' ? length++ : weight++;
                });
              },
              child: Icon(
                FontAwesomeIcons.plus,
                size: 15,
              ),
              borderSide: BorderSide(color: Colors.red),
              highlightColor: Colors.redAccent,
            ),
          ),
          ButtonTheme(
            minWidth: 40,
            child: OutlineButton(
              onPressed: () {
                setState(() {
                  label == 'Length' ? length-- : weight--;
                });
              },
              child: Icon(
                FontAwesomeIcons.minus,
                size: 15,
              ),
              borderSide: BorderSide(color: Colors.red),
              highlightColor: Colors.redAccent,
            ),
          )
        ],
      )
    ]);
  }
}
