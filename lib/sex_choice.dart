import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:health_survey/data.dart';

class SexChoice extends StatelessWidget {
  final Sex sex;
  final IconData icon;

  const SexChoice({Key key, this.sex, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: 40,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          Sex.FEMALE == sex ? "Female" : "Male",
          style: TextStyle(fontSize: 20),
        )
      ],
    );
  }
}
