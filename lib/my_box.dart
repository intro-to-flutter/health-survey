import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBoxWidget extends StatelessWidget {
  final Color color;
  final Widget child;
  final Function onPress;

  MyBoxWidget({this.color = Colors.white, this.child, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.all(12.0),
        child: child,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: color,
        ),
      ),
    );
  }
}
