import 'package:flutter/material.dart';
import 'package:health_survey/data.dart';

class ResultPage extends StatelessWidget {
  static const  String route = "/resultPage";

  @override
  Widget build(BuildContext context) {
    HealthInput healthInput =  ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(title: Text("Health Survey")),
      body: Column(
        children: [
          Expanded(
            child: Text(
              "Expected Lifetime",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: Text(
              LifeTimeCalculator(healthInput).calculate().toString(),
              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ButtonTheme(
                height: 50,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Return Survey", style: TextStyle(fontSize: 20),),
                  color: Colors.white,
                  highlightColor: Colors.redAccent,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
