enum Sex { MALE, FEMALE }

const int averageMaleLifetime = 70;
const int averageFemaleLifetime = 75;

class HealthInput {
  Sex selectedSex;
  double sportRating;
  double smokeRating;
  int length;
  int weight;

  HealthInput(this.selectedSex, this.sportRating, this.smokeRating, this.length,
      this.weight);
}

class LifeTimeCalculator {
  HealthInput healthInput;

  LifeTimeCalculator(this.healthInput);

  int calculate() {
    double result = 0;
    int averageLifetime = healthInput.selectedSex == Sex.FEMALE
        ? averageFemaleLifetime
        : averageMaleLifetime;

    result =
        averageLifetime + (healthInput.sportRating * 2) - (healthInput.smokeRating/2);

    double diffFromIdealIndex = 22.00 -
        ((healthInput.weight / (healthInput.length * healthInput.length)) * 10000);

    result = result - (diffFromIdealIndex.abs() * 1.1);
    return result.round();
  }
}
